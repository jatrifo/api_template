var pg = require("pg");
var types = pg.types;
var config = require("../config.json");
var env = process.env.ENV;



types.setTypeParser(20, function(val){
  return(parseInt(val));
});

types.setTypeParser(1114, function(stringValue) {
return stringValue;
});

var types = {
    FLOAT4: 700,
    FLOAT8: 701,
    NUMERIC: 1700,
    FLOAT4_ARRAY: 1021,
    FLOAT8_ARRAY: 1022,
    NUMERIC_ARRAY: 1231
  };
  //just use the built-in V8 parseFloat implementation
  pg.types.setTypeParser(types.FLOAT4, 'text', parseFloat);
  pg.types.setTypeParser(types.FLOAT8, 'text', parseFloat);
  pg.types.setTypeParser(types.NUMERIC, 'text', parseFloat);


var pool = new pg.Pool(config.database);
var executeQuery = function(query,params,identifier,callback){
  pool.connect(function(err, client, done) {
    if (err){
      console.log("Error en connect");
      console.log(err)
      callback(err,undefined,identifier);
    }
    else {
      client.query(query,params, function(err,result){
        if (err){
          console.log("Error en query");
    	     done();
           callback(err,undefined,identifier);
        }
        else {
          done();
          callback(err,result,identifier);
        }
      });
    }

  })

};
