var express    = require("express");
var app        = express();
var bodyParser = require('body-parser');
var compression = require('compression');
var minify = require('express-minify');
var database = require("./back/database.js");
var queries = require("./back/queries.js");


app.use(bodyParser.urlencoded({ extended: false }));
app.use(compression());
app.use(minify());

var port = process.env.PORT || 6001;

// get an instance of router
var router = express.Router();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", 'GET, POST, PUT, DELETE, PATCH');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Credentials", true);
  next();
});

// Start Server
app.listen(port, function () {
  console.log( "Express server listening on port " + port);
});


app.get("/test",function(request,response){
  console.log("Get Test");
  response.status(200).send({msg:"Hello World"});
  
});
